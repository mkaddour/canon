﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonController : MonoBehaviour
{

    [SerializeField] private GameObject _cannonBall;
    [SerializeField] private Transform _spawn;
    [SerializeField] private int _fireForce;
    [SerializeField] private float _mouseSensitivity = 100.0f; 
    [SerializeField] private float _clampAngle = 80.0f; // 
 
    private float _rotY; // rotation around the up/y axis
    private float _rotX; // rotation around the right/x axis

    private void Start ()
    {
        var rot = transform.localRotation.eulerAngles;
        _rotY = -rot.y;
        _rotX = -rot.x;
        Cursor.visible = false;
    }

    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            var prefab = Instantiate(_cannonBall, _spawn.transform.position,_spawn.transform.rotation);
            prefab.GetComponent<Rigidbody>().AddForce(transform.up * _fireForce,ForceMode.Impulse);
        }
        var mouseX = Input.GetAxis("Mouse X");
        var mouseY = Input.GetAxis("Mouse Y");
 
        _rotY += mouseX * _mouseSensitivity * Time.deltaTime;
        _rotX += mouseY * _mouseSensitivity * Time.deltaTime;
 
        _rotX = Mathf.Clamp(_rotX, -_clampAngle, _clampAngle);
 
        var localRotation = Quaternion.Euler(_rotX, _rotY, 0.0f);
        transform.rotation = localRotation;
    }
}
